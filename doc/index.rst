ANMAT Stock Traceability Module
###############################

This module adds functionality to trace medicaments and interact with ANMAT
(Administración Nacional de Medicamentos, Alimentos y Tecnología Médica)
webservices, in Argentina.
