# -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Not, Eval, Bool
from trytond.pool import PoolMeta

__all__ = ['Product']
__metaclass__ = PoolMeta


class Product:
    'Product'
    __name__ = 'product.product'

    anmat_traceable = fields.Boolean('Is traceable',
        help='Check if the product is traceable by ANMAT')
    anmat_gtin = fields.Char('GTIN',
        states={
            'required': Bool(Eval('anmat_traceable')),
            },
        depends=['anmat_traceable'],
        help='GTIN Number')
    anmat_fractionable = fields.Boolean('Is fractionable',
        help='Check if the product is fractionable')
    anmat_fractions = fields.Integer('Fractions',
        states={
            'required': Bool(Eval('anmat_traceable')),
            'readonly': Not(Bool(Eval('anmat_fractionable'))),
            },
        depends=['anmat_traceable', 'anmat_fractionable'],
        help='If fractionable, fractions must be greater than 1')

    @staticmethod
    def default_anmat_traceable():
        return False

    @staticmethod
    def default_anmat_fractionable():
        return False

    @staticmethod
    def default_anmat_fractions():
        return 1
